# data-fetcher

Python lambda to sync market holidays. Soon to migrate to AWS lambda but must run manually for now.

```sh
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python src/scrapper.py
```

## Lambda setup on AWS

Create necessary AWS resources:

```sh
aws iam create-role --role-name lambda-s3 --assume-role-policy-document file://aws/trust-policy.json
aws iam attach-role-policy --role-name lambda-s3 --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
aws iam attach-role-policy --role-name lambda-s3 --policy-arn arn:aws:iam::aws:policy/AmazonS3FullAccess
```

Package python scrips and libraries into a zip file:

```sh
zip -r -j lambda_function.zip ./src/*
pip install -r requirements.txt --target lib && (cd lib && zip -r ../lambda_function.zip .)
```

Create the lambda:

```sh
export ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text)
aws lambda create-function --function-name run-data-fetcher --zip-file fileb://lambda_function.zip --handler main.handler --runtime python3.8 --role arn:aws:iam::$ACCOUNT_ID:role/lambda-s3 --timeout 900
```

Test lambda:

```sh
aws lambda invoke --function-name run-data-fetcher
```

Update function:

```sh
aws lambda update-function-code --function-name run-data-fetcher --zip-file fileb://lambda_function.zip
```
