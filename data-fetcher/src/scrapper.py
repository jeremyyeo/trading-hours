from bs4 import BeautifulSoup
import requests
import re
import datetime
import json
import string

printable = set(string.printable)

try:
    with open("../src/data/holidays.json") as f:
        previous_data = json.load(f)
        this_exchange = previous_data["NZX"]
        previous_dates = [i["date"] for i in this_exchange]
except:
    previous_data = {}
    this_exchange = []
    previous_dates = []

page = requests.get("https://www.nzx.com/services/nzx-trading/hours-boards")
soup = BeautifulSoup(page.content, "html.parser")
tables = soup.find_all("table")

table_index = -1
new_count = 0

for index, t in enumerate(tables):
    header = t.find_all(re.compile("^h[1-6]"))
    for h in header:
        if "NZX Market Holidays" in h.get_text():
            table_index = index

rows = tables[table_index].find("table").find_all("tr")

for index, r in enumerate(rows):
    if index == 0:
        header = [i.get_text().lower() for i in r]
    else:
        parsed_row = dict(
            zip(
                header,
                ["".join(filter(lambda x: x in printable, i.get_text())) for i in r],
            )
        )
        parsed_row["date"] = datetime.datetime.strptime(
            parsed_row["date"], "%d/%m/%Y"
        ).strftime("%Y-%m-%d")
        if parsed_row["date"] not in previous_dates:
            this_exchange.append(parsed_row)
            new_count += 1

previous_data["NZX"] = this_exchange

with open("../src/data/holidays.json", "w", encoding="utf8") as f:
    json.dump(previous_data, f, indent=4, ensure_ascii=False)

print(f"new dates added: {new_count}")