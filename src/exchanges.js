export const EXCHANGES = [
  {
    name: "NZX",
    flag: "🇳🇿",
    zoneName: "Pacific/Auckland",
    days: [1, 2, 3, 4, 5],
    open: { h: 10, m: 0, s: 0 },
    close: { h: 16, m: 45, s: 0 },
    source: "https://www.nzx.com/services/nzx-trading/hours-boards",
  },
  {
    name: "ASX",
    flag: "🇦🇺",
    zoneName: "Australia/Sydney",
    days: [1, 2, 3, 4, 5],
    open: { h: 10, m: 0, s: 0 },
    close: { h: 16, m: 0, s: 0 },
    source:
      "https://www2.asx.com.au/markets/market-resources/trading-hours-calendar/cash-market-trading-hours",
  },
  {
    name: "NYSE",
    flag: "🇺🇸",
    zoneName: "America/New_York",
    days: [1, 2, 3, 4, 5],
    open: { h: 9, m: 30, s: 0 },
    close: { h: 16, m: 0, s: 0 },
    source: "https://www.nyse.com/markets/hours-calendars",
  },
  {
    name: "NASDAQ",
    flag: "🇺🇸",
    zoneName: "America/New_York",
    days: [1, 2, 3, 4, 5],
    open: { h: 9, m: 30, s: 0 },
    close: { h: 16, m: 0, s: 0 },
    source: "https://www.nasdaq.com/stock-market-trading-hours-for-nasdaq",
  },
];
