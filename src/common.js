export const DATE_FORMAT = "ccc, LLL d, yyyy h:mm:ss a ZZZZZ";
export const DATE_FORMAT_NTZ = "LLL d, yyyy h:mm:ss a";

export function nullify(obj) {
  for (var key in obj) {
    obj[key] = null;
  }
  return obj;
}

export function asSecondOfDay(ts) {
  return (
    parseInt(ts.hour * 60 * 60) + parseInt(ts.minute * 60) + parseInt(ts.second)
  );
}
